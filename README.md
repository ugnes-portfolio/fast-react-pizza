# Learning

- Redux
- React Router
- Tailwind

# How to use 

- Type your name and hit enter. It will appear at the top right of the page
- Select pizzas, quantity. Remove pizzas from your order if you want to
- Go to cart. Change quantities, delete items, clear cart or order pizzas.
- If you wrote your name at login, it will be in the form. Otherwise fill name, phone, add click "Get position" button or write your address.
- You can check priority and order.
- Pizza ingredients are loaded from the menu page.
- You can make your order a priority - price will change and delivery time will reduce.
- You can searh your order number (without #) in search order field 
